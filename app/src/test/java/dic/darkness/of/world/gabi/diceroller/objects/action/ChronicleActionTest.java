package dic.darkness.of.world.gabi.diceroller.objects.action;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class ChronicleActionTest
{
    private ChronicleAction chronicleAction;
    private DiceRoller roller;

    public ChronicleActionTest()
    {
        roller = mock(DiceRoller.class);
        this.chronicleAction = new ChronicleAction(roller);
    }

    /**
     * Tests a successful result and that rerolls can occur.
     */
    @Test
    public void testSuccess1()
    {
        int[] rollResult = {6, 7, 1, 4, 10, 6, 8};

        when(roller.roll(7)).thenReturn(rollResult);
        when(roller.roll()).thenReturn(10).thenReturn(8);

        String result = chronicleAction.standardAction(7);

        String expected = "3 Successes!\nSuccess!";

        verify(roller).roll(7);
        verify(roller, times(2)).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests a successful result and using reroll value less than 10.
     */
    @Test
    public void testSuccess2()
    {
        int[] rollResult = {6, 7, 1, 4, 10, 6, 8};

        when(roller.roll(7)).thenReturn(rollResult);
        when(roller.roll()).thenReturn(10).thenReturn(8).thenReturn(1).thenReturn(6);

        String result = chronicleAction.standardAction(7, 8);

        String expected = "4 Successes!\nSuccess!";

        verify(roller).roll(7);
        verify(roller, times(4)).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests getting only a single success with no rerolls.
     */
    @Test
    public void testSuccess3()
    {
        int[] rollResult = {6, 7, 1, 6, 8};

        when(roller.roll(5)).thenReturn(rollResult);

        String result = chronicleAction.standardAction(5, 10);

        String expected = "1 Success!\nSuccess!";

        verify(roller).roll(5);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests getting a success on the chance die.
     */
    @Test
    public void testSuccess4()
    {
        when(roller.roll()).thenReturn(10);

        String result = chronicleAction.chanceAction();

        String expected = "1 Success!\nSuccess!";

        verify(roller).roll();

        assertEquals(expected,result);
    }

    /**
     * Tests a roll that results in failure.
     */
    @Test
    public void testFailure1()
    {
        int[] rollResult = {6, 7, 1, 4, 2, 6, 7, 4};

        when(roller.roll(8)).thenReturn(rollResult);

        String result = chronicleAction.standardAction(8, 8);

        String expected = "No Successes!\nFailure!";

        verify(roller).roll(8);

        assertEquals(expected,result);
    }

    /**
     * Tests a roll on the chance die that results in failure.
     */
    @Test
    public void testFailure2()
    {
        when(roller.roll()).thenReturn(9);

        String result = chronicleAction.chanceAction();

        String expected = "No Successes!\nFailure!";

        verify(roller).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests an exceptional success based on getting without relying on rerolls.
     */
    @Test
    public void testExceptional1()
    {
        int[] rollResult = {6, 7, 10, 4, 2, 8, 7, 9, 9, 8};

        when(roller.roll(9)).thenReturn(rollResult);
        when(roller.roll()).thenReturn(9);

        String result = chronicleAction.standardAction(9, 10);

        String expected = "5 Successes!!!\nExceptional Success!!!";

        verify(roller).roll(9);
        verify(roller).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests an exceptional success based on rerolls on a 9 and 7 successes.
     */
    @Test
    public void testExceptional2()
    {
        int[] rollResult = {10};

        when(roller.roll(1)).thenReturn(rollResult);
        when(roller.roll()).thenReturn(9).thenReturn(10).thenReturn(9).thenReturn(10)
                .thenReturn(9).thenReturn(10).thenReturn(8);

        String result = chronicleAction.standardAction(1, 9);

        String expected = "7 Successes!!!\nExceptional Success!!!";

        verify(roller).roll(1);
        verify(roller, times(7)).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests a dramatic failure.
     */
    @Test
    public void testDramatic()
    {
        when(roller.roll()).thenReturn(1);

        String result = chronicleAction.chanceAction();

        String expected = "No Successes!\nDramatic Failure!!!";

        verify(roller).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests the reroll method.
     */
    @Test
    public void testReroll()
    {
        when(roller.roll()).thenReturn(10).thenReturn(8);

        int result = chronicleAction.rerollDice(1, 10);

        int expected = 1;

        verify(roller, times(2)).roll();
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }
}

