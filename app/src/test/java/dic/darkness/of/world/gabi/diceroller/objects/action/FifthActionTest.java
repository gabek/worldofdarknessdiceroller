package dic.darkness.of.world.gabi.diceroller.objects.action;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class FifthActionTest
{
    private FifthAction fifthAction;
    private DiceRoller roller;

    public FifthActionTest()
    {
        roller = mock(DiceRoller.class);
        this.fifthAction = new FifthAction(roller);
    }

    @Test
    public void testSuccess1()
    {
        int[] regularResult = {3, 4, 5, 8, 5, 6};
        int[] hungerResult = {1, 10};

        when(roller.roll(6)).thenReturn(regularResult);
        when(roller.roll(2)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(8, 2);

        String expected = "3 Successes!\nSuccess!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(6);
        verify(roller).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }

    @Test
    public void testSuccess2()
    {
        int[] regularResult = {3,8,7,10,5,6};
        int[] hungerResult = {5};

        when(roller.roll(6)).thenReturn(regularResult);
        when(roller.roll(1)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(7, 1, 3);

        String expected = "4 Successes!\nSuccess!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(6);
        verify(roller).roll(1);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }

    @Test
    public void testSuccess3()
    {
        int[] regularResult = {10, 1};
        int[] hungerResult = {1,5};

        when(roller.roll(2)).thenReturn(regularResult).thenReturn(hungerResult);

        String result = fifthAction.standardAction(4, 2, 1);

        String expected = "1 Success!\nSuccess!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller, times(2)).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }

    @Test
    public void testSuccess4()
    {
        int[] hungerResult = {8,8};

        when(roller.roll(2)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(2, 2, 1);

        String expected = "2 Successes!\nSuccess!\nRegular Dice [] & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }



    @Test
    public void testCritcalSuccess1()
    {
        int[] regularResult = {1, 2, 5, 8, 10, 10};
        when(roller.roll(6)).thenReturn(regularResult);

        String result = fifthAction.standardAction(6,0);

        String expected = "5 Successes!!!\nCritical Success!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice []";

        verify(roller).roll(6);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testCriticalSuccess2()
    {
        int[] regularResult = {3, 4, 10, 10, 10};
        when(roller.roll(5)).thenReturn(regularResult);

        String result = fifthAction.standardAction(5, 0);

        String expected = "5 Successes!!!\nCritical Success!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice []";

        verify(roller).roll(5);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testCriticalSuccess3()
    {
        int[] regularResult = {8, 4, 10, 10};
        int[] hungerResult = {1, 8};

        when(roller.roll(4)).thenReturn(regularResult);
        when(roller.roll(2)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(6, 2, 4);

        String expected = "6 Successes!!!\nCritical Success!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verify(roller).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testFailure1()
    {
        int[] regularResult = {1,2,4,5};
        int[] hungerResult = {3};

        when(roller.roll(4)).thenReturn(regularResult);
        when(roller.roll(1)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(5, 1);

        String expected = "No Successes.\nFailure!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verify(roller).roll(1);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testFailure2()
    {
        int[] regularResult = {10,2,1,10};
        int[] hungerResult = {3};

        when(roller.roll(4)).thenReturn(regularResult);
        when(roller.roll(1)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(5, 1, 5);

        String expected = "4 Successes.\nFailure!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verify(roller).roll(1);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testFailure3()
    {
        int[] hungerResult = {8, 3, 2, 5};

        when(roller.roll(4)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(4, 4, 4);

        String expected = "1 Success.\nFailure!\nRegular Dice [] & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testBestialFailure1()
    {
        int[] regularResult = {2, 10};
        int[] hungerResult = {1, 1};

        when(roller.roll(2)).thenReturn(regularResult).thenReturn(hungerResult);

        String result = fifthAction.standardAction(4,2,3);

        String expected = "1 Success.\nBestial Failure!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller, times(2)).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }


    @Test
    public void testBestialFailure2()
    {
        int[] regularResult = {3,2,2,10};
        int[] hungerResult = {1, 1, 10};

        when(roller.roll(4)).thenReturn(regularResult);
        when(roller.roll(3)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(7,3,6);

        String expected = "4 Successes.\nBestial Failure!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verify(roller).roll(3);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }

    @Test
    public void testBestialFailure3()
    {
        int[] hungerResult = {1, 1};

        when(roller.roll(2)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(2,3,6);

        String expected = "No Successes.\nBestial Failure!!!\nRegular Dice [] & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected, result);
    }


    @Test
    public void testMessyCritical1()
    {
        int[] regularResult = {10,5,4, 1};
        int[] hungerResult = {6, 10};

        when(roller.roll(4)).thenReturn(regularResult);
        when(roller.roll(2)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(6, 2, 5);

        String expected = "5 Successes!!!\nMessy Critical!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verify(roller).roll(2);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testMessyCritical2()
    {
        int[] regularResult = {10,5};
        int[] hungerResult = {1, 10, 1};

        when(roller.roll(2)).thenReturn(regularResult);
        when(roller.roll(3)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(5, 3, 3);

        String expected = "4 Successes!!!\nMessy Critical!!!\nRegular Dice " + Arrays.toString(regularResult) +" & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(2);
        verify(roller).roll(3);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    @Test
    public void testMessyCritical3()
    {
        int[] hungerResult = {10, 10, 10, 10};

        when(roller.roll(4)).thenReturn(hungerResult);

        String result = fifthAction.standardAction(4, 5, 5);

        String expected = "8 Successes!!!\nMessy Critical!!!\nRegular Dice [] & Hunger Dice " + Arrays.toString(hungerResult);

        verify(roller).roll(4);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }
}
