package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Tests the ClassicShortcutHandler object.
 */
public class ClassicShortcutHandlerTest
{
    private Context context;
    private FileOutputStream outputStream;
    private ClassicShortcutHandler shortcutHandler;
    private final String FILENAME = "classic_shortcut.json";

    public ClassicShortcutHandlerTest()
    {
        this.context = mock(Context.class);
        outputStream = mock(FileOutputStream.class);
        shortcutHandler = new ClassicShortcutHandler(context, FILENAME, new ObjectMapper());
    }

    /**
     * Tests successfully reading from a file.
     */
    @Test
    public void testReadFile1()
    {
        try {
            URL resource = getClass().getClassLoader().getResource(FILENAME);
            File file = new File(resource.toURI());
            FileInputStream inputStream = new FileInputStream(file);
            when(context.openFileInput(FILENAME)).thenReturn(inputStream);

            HashMap<String, ClassicShortcut> result = shortcutHandler.readFile();

            HashMap<String, ClassicShortcut> expected = new HashMap<>();
            expected.put("Alpha", new ClassicShortcut(8, 4));
            expected.put("Beta", new ClassicShortcut(3, 7));

            verify(context).openFileInput(FILENAME);
            verifyNoMoreInteractions(context);

            for (String key : expected.keySet()) {
                assertTrue(result.containsKey(key));
                assertTrue(expected.get(key).equals(result.get(key)));
            }
        }
        catch (IOException e)
        {
            fail("Exception thrown: " + e);
        }
        catch (URISyntaxException e)
        {
            fail("Exception thrown: " + e);
        }
    }

    /**
     * Tests failing to read from a file and properly handling the error.
     * @throws FileNotFoundException
     */
    @Test
    public void testReadFile2() throws FileNotFoundException
    {
        FileNotFoundException exception = new FileNotFoundException();
        when(context.openFileInput(FILENAME)).thenThrow(exception);

        HashMap<String, ClassicShortcut> result = shortcutHandler.readFile();

        assertTrue(result.isEmpty());

        verify(context).openFileInput(FILENAME);
        verifyNoMoreInteractions(context);
    }

    /**
     * Tests successfully writing to a file.
     */
    @Test
    public void testWriteFile1()
    {
        try {
            when(context.openFileOutput(FILENAME, Context.MODE_PRIVATE)).thenReturn(outputStream);

            HashMap<String, ClassicShortcut> map = new HashMap<>();
            map.put("Alpha", new ClassicShortcut(8, 4));
            map.put("Beta", new ClassicShortcut(3, 7));

            String json = new ObjectMapper().writeValueAsString(map);

            boolean result = shortcutHandler.writeFile(map);

            assertTrue(result);
            verify(context).openFileOutput(FILENAME, Context.MODE_PRIVATE);
            verifyNoMoreInteractions(context);

            verify(outputStream).close();
            verify(outputStream).write(json.getBytes());
            verifyNoMoreInteractions(outputStream);
        }
        catch (IOException e)
        {
            fail("IOException thrown: " + e);
        }
    }

    /**
     * Tests failing to write to a file and successful error handling of it.
     */
    @Test
    public void testWriteFile2()
    {
        try {
            FileNotFoundException exception = new FileNotFoundException();
            when(context.openFileOutput(FILENAME, Context.MODE_PRIVATE)).thenThrow(exception);

            HashMap<String, ClassicShortcut> map = new HashMap<>();
            map.put("Alpha", new ClassicShortcut(8, 4));
            map.put("Beta", new ClassicShortcut(3, 7));

            boolean result = shortcutHandler.writeFile(map);

            assertFalse(result);

            verify(context).openFileOutput(FILENAME, Context.MODE_PRIVATE);
            verifyNoMoreInteractions(context);

            verifyZeroInteractions(outputStream);
        }
        catch (IOException e)
        {
            fail("IOException thrown: " + e);
        }
    }

    /**
     * Testing writing to a file with non classic shortcuts passed to the method.
     */
    @Test
    public void testWriteFile3()
    {
        try {
            when(context.openFileOutput(FILENAME, Context.MODE_PRIVATE)).thenReturn(outputStream);

            HashMap<String, Shortcut> map = new HashMap<>();
            map.put("Alpha", new ClassicShortcut(8, 4));
            map.put("Beta", new ClassicShortcut(3, 7));

            String json = new ObjectMapper().writeValueAsString(map);

            boolean result = shortcutHandler.writeFile(map);

            map.put("Charlie", new ChronicleShortcut(6,10));

            assertTrue(result);
            verify(context).openFileOutput(FILENAME, Context.MODE_PRIVATE);
            verifyNoMoreInteractions(context);

            verify(outputStream).close();
            verify(outputStream).write(json.getBytes());
            verifyNoMoreInteractions(outputStream);
        }
        catch (IOException e)
        {
            fail("IOException thrown: " + e);
        }
    }
}
