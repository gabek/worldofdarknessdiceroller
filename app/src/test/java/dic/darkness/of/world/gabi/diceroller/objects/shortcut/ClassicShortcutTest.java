package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the ClassicShortcut object.
 */
public class ClassicShortcutTest
{
    /**
     * Tests the overidden equals method.
     */
    @Test
    public void testEquals()
    {
        ClassicShortcut shortcut1 = new ClassicShortcut(7,6);
        ClassicShortcut shortcut2 = new ClassicShortcut(7, 6);
        ClassicShortcut shortcut3 = new ClassicShortcut(4, 6);
        ClassicShortcut shortcut4 = new ClassicShortcut(7, 5);


        Shortcut classicStill = new ClassicShortcut(7,6);
        Shortcut notClassic = new ChronicleShortcut(7,6);

        Object rando = new Object();

        boolean result1 = shortcut1.equals(shortcut1);
        boolean result2 = shortcut1.equals(shortcut2);
        boolean result3 = shortcut1.equals(shortcut3);
        boolean result4 = shortcut1.equals(shortcut4);
        boolean result5 = shortcut1.equals(classicStill);
        boolean result6 = shortcut1.equals(rando);
        boolean result7 = shortcut1.equals(notClassic);

        assertTrue(result1);
        assertTrue(result2);
        assertFalse(result3);
        assertFalse(result4);
        assertTrue(result5);
        assertFalse(result6);
        assertFalse(result7);
    }
}
