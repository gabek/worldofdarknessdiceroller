package dic.darkness.of.world.gabi.diceroller.objects.action;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class ClassicActionTest
{
    private ClassicAction classicAction;
    private DiceRoller roller;

    public ClassicActionTest()
    {
        roller = mock(DiceRoller.class);
        this.classicAction = new ClassicAction(roller);
    }

    /**
     * Tests a success that uses the standard difficulty and reduces successes through 1s.
     */
    @Test
    public void testSuccess1()
    {
        int[] rollResult = {6, 7, 1, 3, 10, 6, 8};
        when(roller.roll(7)).thenReturn(rollResult);
        String result = classicAction.standardAction(7);
        String expected = "4 Successes!\nSuccess!";

        verify(roller).roll(7);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests a roll that has a difficulty of 4 and reduces it successes
     * by rolling a 1.
     */
    @Test
    public void testSuccess2()
    {
        int[] rollResult = {4,3,4,1,4};
        when(roller.roll(5)).thenReturn(rollResult);
        String result = classicAction.standardAction(5, 4);
        String expected = "2 Successes!\nSuccess!";

        verify(roller).roll(5);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests a singular success.
     */
    @Test
    public void testSuccess3()
    {
        int[] rollResult = {6,3,7,10};
        when(roller.roll(4)).thenReturn(rollResult);
        String result = classicAction.standardAction(4, 9);
        String expected = "1 Success!\nSuccess!";
        assertEquals(expected,result);
    }

    /**
     * Tests a critical sucess with 6 succeses.
     */
    @Test
    public void testCriticalSuccess1()
    {
        int[] rollResult = {6,3,7,10, 10, 2, 5, 6, 7};
        when(roller.roll(9)).thenReturn(rollResult);
        String result = classicAction.standardAction(9, 6);
        String expected = "6 Successes!!!\nCritical Success!!!";

        verify(roller).roll(9);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests a critical success with 5 successes.
     */
    @Test
    public void testCriticalSuccess2()
    {
        int[] rollResult = {6,1,7,10, 10, 2, 5, 6, 7};
        when(roller.roll(9)).thenReturn(rollResult);
        String result = classicAction.standardAction(9, 6);
        String expected = "5 Successes!!!\nCritical Success!!!";

        verify(roller).roll(9);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }


    /**
     * Tests a roll that results in a failure due to no successes earned.
     */
    @Test
    public void testFailure1()
    {
        int[] rollResult = {4,3,2,5,4};
        when(roller.roll(5)).thenReturn(rollResult);
        String result = classicAction.standardAction(5, 6);
        String expected = "No Successes!\nFailure!";

        verify(roller).roll(5);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests in a roll that results in a failure due the number of
     * 1's being greater than the number of successes.
     */
    @Test
    public void testFailure2()
    {
        int[] rollResult = {4,3,1,1,4,6};
        when(roller.roll(6)).thenReturn(rollResult);
        String result = classicAction.standardAction(6, 6);
        String expected = "No Successes!\nFailure!";

        verify(roller).roll(6);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }

    /**
     * Tests a roll that results in a botch.
     */
    @Test
    public void testBotch()
    {
        int[] rollResult = {4,3,2,1,4,6};
        when(roller.roll(6)).thenReturn(rollResult);
        String result = classicAction.standardAction(6, 8);
        String expected = "No Successes!\nBotch!!!";

        verify(roller).roll(6);
        verifyNoMoreInteractions(roller);

        assertEquals(expected,result);
    }
}
