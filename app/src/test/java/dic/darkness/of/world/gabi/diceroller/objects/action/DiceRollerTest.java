package dic.darkness.of.world.gabi.diceroller.objects.action;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class DiceRollerTest
{
    @Test
    public void testDiceRoller()
    {
        DiceRoller roller = new DiceRoller();


        int resultSize1 = roller.roll(1).length;
        int resultSize2 = roller.roll(2).length;
        int resultSize3 = roller.roll(3).length;
        int resultSize4 = roller.roll(4).length;
        int resultSize5 = roller.roll(5).length;
        int resultSize6 = roller.roll(6).length;
        int resultSize7 = roller.roll(7).length;
        int resultSize8 = roller.roll(8).length;
        int resultSize9 = roller.roll(9).length;
        int resultSize10 = roller.roll(10).length;

        int[] result = roller.roll(10000);
        List<Integer> resultList = IntStream.of(result).boxed().collect(Collectors.<Integer>toList());
        int resultMax = Collections.max(resultList);
        int resultMin = Collections.min(resultList);

        assertEquals(1, resultSize1);
        assertEquals(2, resultSize2);
        assertEquals(3, resultSize3);
        assertEquals(4, resultSize4);
        assertEquals(5, resultSize5);
        assertEquals(6, resultSize6);
        assertEquals(7, resultSize7);
        assertEquals(8, resultSize8);
        assertEquals(9, resultSize9);
        assertEquals(10, resultSize10);

        assertEquals(10, resultMax);
        assertEquals(1, resultMin);
    }
}
