package dic.darkness.of.world.gabi.diceroller.objects.action;

/**
 * An Action in Classic World of Darkness.
 */
public class ClassicAction implements Action
{
    private DiceRoller diceRoller;

    public ClassicAction()
    {
        diceRoller = new DiceRoller();
    }

    public ClassicAction(DiceRoller diceRoller)
    {
        this.diceRoller = diceRoller;
    }

    /**
     * A standard action. Will roll a set number of dice against a difficulty of 6.
     *
     * @param diceNum The number of dice to be rolled.
     * @return String The result of the roll as a string.
     */
    public String standardAction(int diceNum)
    {
        return standardAction(diceNum, 6, 0);
    }

    /**
     * A standard action that takes a set number of dice and a specified difficulty number.
     * @param diceNum The number of dice to be rolled.
     * @param diffNum The difficulty of the roll.
     * @return String the result of the Roll as a String.
     */
    public String standardAction(int diceNum, int diffNum)
    {
        return standardAction(diceNum, diffNum, 0);
    }

    public String standardAction(int diceNum, int diffNum, int willpower)
    {
        int[] result = diceRoller.roll(diceNum);
        int successes = 0;
        int botch = 0;
        for (int i : result)
        {
            if(i>=diffNum)
            {
                successes++;
                botch =1;
            }
            if(i==1)
            {
                successes--;
                if(botch==0)
                    botch=-1;
            }
        }

        if(successes < 0)
            successes = 0;

        successes = successes + willpower;
        return checkSuccesses(successes, botch);
    }

    /**
     * Checks the number of successes in a roll and outputs a result. In charge of determining botches.
     *
     * @param successes The number of successes the roll gained.
     * @param botch An int to determine if a botch occurred. Botch is -1, 0 means no successes were
     *              gained but no ones, and a 1 means at least successes occurred.
     * @return String The result of the roll as a string.
     */
    private String checkSuccesses(int successes, int botch)
    {
        if(successes<=0)
        {
            String out = "No Successes!\n";
            if(botch==-1)
                return out + Result.Botch.getValue() +"!!!";
            else
                return out + Result.Failure.getValue() + "!";
        }
        else if(successes<5)
        {
            if(successes==1)
                return "1 Success!\n" + Result.Success.getValue() + "!";
            else
                return successes + " Successes!\n" + Result.Success.getValue() + "!";
        }
        else
            return successes + " Successes!!!\n" + Result.Critical.getValue() + "!!!";
    }

}
