package dic.darkness.of.world.gabi.diceroller.chronicle;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dic.darkness.of.world.gabi.diceroller.MainActivity;
import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.action.ChronicleAction;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ChronicleShortcut;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ChronicleShortcutHandler;

/**
 * The Activity for actions in Chronicles of Darkness.
 */
public class ChroniclesRollerActivity extends AppCompatActivity {

    private static final String FILENAME = "chronicles_shortcut.json";
    private boolean chance = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chronicles_roller);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ChronicleShortcutHandler shortcutHandler = new ChronicleShortcutHandler(this, FILENAME, new ObjectMapper());

        final TextView textView = findViewById(R.id.instructions);

        final NumberPicker diceNumber = findViewById(R.id.dice_num);
        diceNumber.setMaxValue(50);
        diceNumber.setMinValue(1);
        diceNumber.setValue(6);

        final NumberPicker rerollOn = findViewById(R.id.reroll_num);
        rerollOn.setMaxValue(10);
        rerollOn.setMinValue(8);
        rerollOn.setValue(10);

        final Spinner dropdown = findViewById(R.id.spinner_shortcut);

        HashMap<String, ChronicleShortcut> savedShortcuts = shortcutHandler.readFile();

        final HashMap<String, ChronicleShortcut> finalShortcuts = savedShortcuts;
        String[] items = createSpinnerList(finalShortcuts);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String shortcutName = parent.getItemAtPosition(position).toString();
                if(shortcutName.equals("Chance Die"))
                {
                    diceNumber.setEnabled(false);
                    rerollOn.setEnabled(false);
                    chance = true;
                }
                else if(finalShortcuts.containsKey(shortcutName))
                {
                    chance = false;
                    ChronicleShortcut shortcut = finalShortcuts.get(shortcutName);

                    if(!diceNumber.isEnabled())
                        diceNumber.setEnabled(true);
                    diceNumber.setValue(shortcut.getDiceNum());

                    if(!rerollOn.isEnabled())
                        rerollOn.setEnabled(true);
                    rerollOn.setValue(shortcut.getReroll());

                    diceNumber.setEnabled(false);
                    rerollOn.setEnabled(false);
                }
                else
                {
                    chance = false;
                    diceNumber.setEnabled(true);
                    rerollOn.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                diceNumber.setEnabled(true);
                rerollOn.setEnabled(true);
            }
        });

        final Button button = findViewById(R.id.roll_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){

                ChronicleAction chronicleAction = new ChronicleAction();

                String text;
                if(chance)
                {
                    text = chronicleAction.chanceAction();
                }
                else
                {
                    int diceNum = diceNumber.getValue();
                    int reroll = rerollOn.getValue();
                    text = chronicleAction.standardAction(diceNum, reroll);
                }
                textView.setText(text);

            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_roller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_link) {
            Intent intent = new Intent(getApplicationContext(), ChronicleSaveActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.delete_link)
        {
            Intent intent = new Intent(getApplicationContext(), ChronicleDeleteActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.main_link)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    protected String[] createSpinnerList(HashMap<String, ChronicleShortcut> shortCuts)
    {
        String[] out = new String[shortCuts.size()+2];
        out[0] = "";
        out[1] = "Chance Die";
        List<String> keys = new ArrayList<String>(shortCuts.keySet());
        for(int i = 2; i < out.length; i++)
            out[i] = keys.get(i-2);
        return out;
    }

}
