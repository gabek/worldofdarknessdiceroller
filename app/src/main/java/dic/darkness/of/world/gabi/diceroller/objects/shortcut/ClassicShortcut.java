package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * A shortcut for classic world of darkness.
 * Implements Shortcut.
 */
public class ClassicShortcut implements Shortcut
{
    @JsonProperty("diceNum")
    private int diceNum;

    @JsonProperty("difficulty")
    private int difficulty;

    public ClassicShortcut()
    {
    }
    public ClassicShortcut(int diceNum, int difficulty)
    {
        this.diceNum = diceNum;
        this.difficulty = difficulty;
    }

    public int getDiceNum()
    {
        return diceNum;
    }

    public int getDifficulty()
    {
        return  difficulty;
    }

    @Override
    public boolean equals(Object object)
    {
        if(this==object)
            return true;
        ClassicShortcut that;
        if(object instanceof ClassicShortcut)
            that = (ClassicShortcut) object;
        else
            return false;

        if (this.getDiceNum()!= that.getDiceNum())
            return false;

        return this.getDifficulty() == that.getDifficulty();
    }
}
