package dic.darkness.of.world.gabi.diceroller.chronicle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dic.darkness.of.world.gabi.diceroller.MainActivity;
import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ChronicleShortcut;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ChronicleShortcutHandler;

/**
 * The Activity for deleting shortcuts in Chronicle of Darkness.
 */
public class ChronicleDeleteActivity extends AppCompatActivity {

    private final String FILENAME = "chronicles_shortcut.json";
    HashMap<String, ChronicleShortcut> savedShortcuts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chronicle_delete);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ChronicleShortcutHandler shortcutHandler = new ChronicleShortcutHandler(this, FILENAME, new ObjectMapper());

        final Spinner dropdown = findViewById(R.id.spinner_shortcut);

        savedShortcuts = shortcutHandler.readFile();
        String[] items = createSpinnerList(savedShortcuts);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        final Button button = findViewById(R.id.delete_button);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setTitle("Deleting " + dropdown.getSelectedItem());
        alertDialog.setMessage("Are you sure you want to delete this shortcut?");

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String shortcutName = (String) dropdown.getSelectedItem();
                if(savedShortcuts.containsKey(shortcutName))
                {
                    savedShortcuts.remove(shortcutName);

                    if(shortcutHandler.writeFile(savedShortcuts))
                    {
                        String[] items = createSpinnerList(savedShortcuts);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, items);
                        dropdown.setAdapter(adapter);

                        Toast toast = Toast.makeText(getApplicationContext(), "Deleted: " + shortcutName, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    else
                    {
                        Toast toast = Toast.makeText(getApplicationContext(), "Failed to delete " + shortcutName, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Error: " + shortcutName + " is missing.\nRemoving " + shortcutName + " from dropdown.",
                            Toast.LENGTH_SHORT);
                    toast.show();

                    String[] items = createSpinnerList(savedShortcuts);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, items);
                    dropdown.setAdapter(adapter);
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!dropdown.getSelectedItem().equals(""))
                {
                    alertDialog.show();
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please select an item to delete",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_link) {
            Intent intent = new Intent(getApplicationContext(), ChronicleSaveActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.roller_link)
        {
            Intent intent = new Intent(getApplicationContext(), ChroniclesRollerActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.main_link)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    protected String[] createSpinnerList(HashMap<String, ChronicleShortcut> shortCuts)
    {
        String[] out = new String[shortCuts.size()+1];
        out[0] ="";
        List<String> keys = new ArrayList<String>(shortCuts.keySet());
        for(int i = 1; i < out.length; i++)
            out[i] = keys.get(i-1);
        return out;
    }

}
