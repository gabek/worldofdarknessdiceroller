package dic.darkness.of.world.gabi.diceroller.classic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;


import java.util.HashMap;

import dic.darkness.of.world.gabi.diceroller.MainActivity;
import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ClassicShortcut;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ClassicShortcutHandler;

/**
 * The activity for creating a Shortcut for Classic World of Darkness.
 */

public class ClassicSaveActivity extends AppCompatActivity {
    private static final String FILENAME = "classic_shortcut.json";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        final ClassicShortcutHandler shortcutHandler = new ClassicShortcutHandler(this, FILENAME, new ObjectMapper());


        setContentView(R.layout.activity_classic_save);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final EditText nameText = findViewById(R.id.shortcut_name);

        final NumberPicker dice = findViewById(R.id.dice_num);
        dice.setMinValue(1);
        dice.setMaxValue(50);
        dice.setValue(6);

        final NumberPicker difficulty = findViewById(R.id.diff_num);
        difficulty.setMinValue(1);
        difficulty.setMaxValue(10);
        difficulty.setValue(6);

        final Button save = findViewById(R.id.save_button);
        save.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                if(nameText.getText()!= null&&!nameText.getText().equals(""))
                {
                    int diceNum = dice.getValue();
                    int diffNum = difficulty.getValue();
                    String name = nameText.getText().toString();

                    if(name.equals(""))
                    {
                        Toast toast = Toast.makeText(getApplicationContext(), "Please input a name for the Shortcut.",
                                Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }

                    HashMap<String, ClassicShortcut> map = shortcutHandler.readFile();
                    ClassicShortcut shortcut = new ClassicShortcut(diceNum, diffNum);
                    map.put(name, shortcut);

                    if(shortcutHandler.writeFile(map))
                    {
                        Toast toast = Toast.makeText(getApplicationContext(), "Saved: " + name,
                            Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    else
                    {
                        Toast toast = Toast.makeText(getApplicationContext(), "Input/Output error. Failed to save " + name,
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    nameText.setText("");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.roller_link) {
            Intent intent = new Intent(getApplicationContext(), ClassicRollerActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.delete_link)
        {
            Intent intent = new Intent(getApplicationContext(), ClassicDeleteActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.main_link)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
