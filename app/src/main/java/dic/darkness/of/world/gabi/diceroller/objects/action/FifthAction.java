package dic.darkness.of.world.gabi.diceroller.objects.action;

import java.util.Arrays;

public class FifthAction implements Action
{

    private DiceRoller diceRoller;

    public FifthAction()
    {
        this.diceRoller = new DiceRoller();
    }

    public FifthAction(DiceRoller diceRoller)
    {
        this.diceRoller = diceRoller;
    }

    @Override
    public String standardAction(int diceNum)
    {
        return standardAction(diceNum, 1, 1);
    }

    public String standardAction(int diceNum, int hunger)
    {
        return standardAction(diceNum, hunger,1);
    }

    public String standardAction(int diceNum, int hunger, int difficulty)
    {
        int regular = diceNum - hunger;
        regular = Math.max(regular, 0);
        int hungerDice = Math.min(diceNum, hunger);
        int successes = 0;
        boolean bestial = false;
        int critical = 0;
        int tens = 0;
        int[] normalResult = new int[0];
        int[] hungerResult = new int[0];

        if (regular > 0)
        {
             normalResult = diceRoller.roll(regular);

            for (int i : normalResult)
            {
                if (i >= 6)
                {
                    successes++;
                    if (i == 10)
                    {
                        tens++;
                        if(tens >= 2)
                            critical = 1;
                    }

                }
            }
        }

        if (hungerDice > 0)
        {
            hungerResult = diceRoller.roll(hungerDice);

            for (int i : hungerResult)
            {
                if (i >= 6)
                {
                    successes++;
                    if (i == 10)
                    {
                        tens++;
                        if(tens >= 2)
                            critical = -1;
                    }


                }
                else if (i == 1)
                    bestial = true;
            }
        }

        successes = successes + ((tens / 2) * 2);
        return checkSuccesses(normalResult, hungerResult, successes, critical, bestial, difficulty);
    }

    private String checkSuccesses(int[] normalResult, int[] hungerResult, int successes, int critical, boolean bestial, int difficulty)
    {
        String result = "";
        if(successes>=difficulty)
        {
            if(critical>0)
                result = successes + " Successes!!!\n" + Result.Critical.getValue() + "!!!";
            else if(critical<0)
                result = successes + " Successes!!!\n" + Result.MessyCritical.getValue() + "!!!";
            else if(successes==1)
                result = "1 Success!\n" + Result.Success.getValue() + "!";
            else
                result = successes + " Successes!\n" + Result.Success.getValue() + "!";
        }
        else
        {
            String firstHalf = "";
            if(successes == 0)
                firstHalf = "No Successes.\n";
            else if(successes == 1)
                firstHalf = "1 Success.\n";
            else
                firstHalf = successes + " Successes.\n";

            if(bestial)
            {
                result =  firstHalf + Result.BestialFailure.getValue() + "!!!";
            }
            else
            {
                result = firstHalf + Result.Failure.getValue() + "!";
            }
        }

        return result + "\nRegular Dice " + Arrays.toString(normalResult) +" & Hunger Dice " + Arrays.toString(hungerResult);
    }
}
