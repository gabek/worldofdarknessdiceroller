package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FifthShortcut implements Shortcut
{
    @JsonProperty("regularDice")
    private int regularDice;

    @JsonProperty("hungerDice")
    private int hungerDice;

    @JsonProperty("difficulty")
    private int difficulty;

    public FifthShortcut()
    {
    }

    public FifthShortcut(int regularDice, int hungerDice, int difficulty)
    {
        this.regularDice = regularDice;
        this.hungerDice = hungerDice;
        this.difficulty = difficulty;
    }

    public int getDiceNum()
    {
        return regularDice;
    }

    public int getHungerDice()
    {
        return hungerDice;
    }

    public int getDifficulty()
    {
        return difficulty;
    }

    @Override
    public boolean equals(Object object)
    {
        if(this==object) return true;

        FifthShortcut that;

        if(object instanceof FifthShortcut)
            that = (FifthShortcut) object;
        else
            return false;

        if(this.regularDice != that.regularDice)
            return false;
        else if(this.hungerDice != that.hungerDice)
            return false;
        else
            return this.difficulty == that.difficulty;
    }
}
