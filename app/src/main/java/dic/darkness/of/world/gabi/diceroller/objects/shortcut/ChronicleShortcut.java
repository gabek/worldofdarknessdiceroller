package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A shortcut for Chronicle of Darkness games.
 */
public class ChronicleShortcut implements Shortcut
{
    @JsonProperty("diceNum")
    private int diceNum;

    @JsonProperty("reroll")
    private int reroll;

    public ChronicleShortcut()
    {

    }

    public ChronicleShortcut(int diceNum, int reroll)
    {
        this.diceNum = diceNum;
        this.reroll = reroll;
    }

    public int getDiceNum()
    {
        return diceNum;
    }
    public int getReroll()
    {
        return reroll;
    }

    @Override
    public boolean equals(Object object)
    {
        if(this == object) return true;

        ChronicleShortcut that;

        if(object instanceof ChronicleShortcut)
            that = (ChronicleShortcut) object;
        else
            return false;

        if(this.diceNum != that.diceNum)
            return false;
        else
            return this.reroll == that.reroll;
    }
}
