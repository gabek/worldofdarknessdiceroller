package dic.darkness.of.world.gabi.diceroller.objects.action;

/**
 * An Interface for a standard action in World of Darkness systems.
 */
public interface Action
{
    public String standardAction(int diceNum);

    public String standardAction(int diceNum, int mod);

}
