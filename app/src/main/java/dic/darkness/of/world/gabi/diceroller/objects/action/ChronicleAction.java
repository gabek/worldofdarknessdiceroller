package dic.darkness.of.world.gabi.diceroller.objects.action;

/**
 * An action in Chronicles of Darkness.
 */
public class ChronicleAction implements Action
{
    private DiceRoller diceRoller;

    public ChronicleAction()
    {
        this.diceRoller = new DiceRoller();
    }

    public ChronicleAction(DiceRoller diceRoller)
    {
        this.diceRoller = diceRoller;
    }

    /**
     * Roll a number of dies that will reroll on a 10.
     *
     * @param dice The number of dice to be rolled.
     * @return String The result of the roll as a string.
     */
    public String standardAction(int dice)
    {
        return standardAction(dice, 10);
    }

    /**
     * Represents a roll on a chance die.
     *
     * @return String The result of the roll as a String.
     */
    public String chanceAction()
    {
        int result = diceRoller.roll();
        if(result==10) return checkSuccesses(1, false);
        else if(result==1) return checkSuccesses(0,true);
        else return checkSuccesses(0,false);
    }

    /**
     * A standard action that rolls a set number of dice, and will reroll on a specified number.
     *
     * @param dice The number of dice to be rolled.
     * @param reroll The number to reroll a die on.
     * @return
     */
    public String standardAction(int dice, int reroll)
    {
        int[] result = diceRoller.roll(dice);
        int numRolls = 0;
        int successes = 0;

        for(int i : result)
        {
            if(i >= 8)
            {
                successes++;
                if (i >= reroll)
                    numRolls++;
            }
        }
        if(numRolls>0)
            successes = successes + rerollDice(numRolls, reroll);
        return checkSuccesses(successes, false);
    }

    /**
     * Rerolls a set number of dice until there are no more dice to reroll.
     *
     * @param numRolls The number of dice to be rerolled.
     * @param reroll The number that must be met to reroll on.
     * @return int The Number of successes gained through rerolling dice.
     */
    protected int rerollDice(int numRolls, int reroll)
    {
        int successes = 0;
        while (numRolls>0)
        {
            int result = diceRoller.roll();
            if(result >= reroll)
                successes++;
            else
                numRolls--;
        }
        return successes;
    }

    /**
     * Checks the result of the roll.
     *
     * @param successes The number of successes gained .
     * @param dramatic Is this a dramatic failure.
     * @return String Representing the result of the successes totalled.
     */
    private String checkSuccesses(int successes, boolean dramatic)
    {
        if(dramatic)
            return "No Successes!\n" + Result.Dramatic.getValue() + "!!!";
        else if(successes==0)
            return "No Successes!\n" + Result.Failure.getValue() + "!";
        else
        {
            if(successes == 1)
                return "1 Success!\n" + Result.Success.getValue() + "!";
            else if(successes<5)
                return successes + " Successes!\n" + Result.Success.getValue() + "!";
            else
                return successes + " Successes!!!\n" + Result.Exceptional.getValue() + "!!!";
        }

    }
}
