package dic.darkness.of.world.gabi.diceroller.classic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dic.darkness.of.world.gabi.diceroller.MainActivity;
import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.action.ClassicAction;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ClassicShortcut;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ClassicShortcutHandler;

/**
 * The Activity for Actions in Classic World of Darkness.
 */

public class ClassicRollerActivity extends AppCompatActivity  {

    private static final String FILENAME = "classic_shortcut.json";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ClassicShortcutHandler shortcutHandler = new ClassicShortcutHandler(this, FILENAME, new ObjectMapper());

        setContentView(R.layout.activity_classic_roller);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final TextView textView = findViewById(R.id.instructions);

        final NumberPicker diceNumber = findViewById(R.id.dice_num);
        diceNumber.setMaxValue(50);
        diceNumber.setMinValue(1);
        diceNumber.setValue(6);

        final NumberPicker difficulty = findViewById(R.id.diff_num);
        difficulty.setMaxValue(10);
        difficulty.setMinValue(1);
        difficulty.setValue(6);

        final Spinner dropdown = findViewById(R.id.spinner_shortcut);

        HashMap<String, ClassicShortcut> savedShortcuts = shortcutHandler.readFile();

        final HashMap<String, ClassicShortcut> finalShortcuts = savedShortcuts;
        String[] items = createSpinnerList(finalShortcuts);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String shortcutName = parent.getItemAtPosition(position).toString();
                if(finalShortcuts.containsKey(shortcutName))
                {

                    ClassicShortcut shortcut = finalShortcuts.get(shortcutName);

                    if(!diceNumber.isEnabled())
                        diceNumber.setEnabled(true);
                    diceNumber.setValue(shortcut.getDiceNum());

                    if(!difficulty.isEnabled())
                        difficulty.setEnabled(true);
                    difficulty.setValue(shortcut.getDifficulty());

                    diceNumber.setEnabled(false);
                    difficulty.setEnabled(false);
                }
                else
                {
                    diceNumber.setEnabled(true);
                    difficulty.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                diceNumber.setEnabled(true);
                difficulty.setEnabled(true);
            }
        });

        final Button button = findViewById(R.id.roll_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                ClassicAction classicAction = new ClassicAction();

                int diceNum = diceNumber.getValue();
                int diffNum = difficulty.getValue();
                String text = classicAction.standardAction(diceNum, diffNum);
                textView.setText(text);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_roller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_link) {
            Intent intent = new Intent(getApplicationContext(), ClassicSaveActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.delete_link)
        {
            Intent intent = new Intent(getApplicationContext(), ClassicDeleteActivity.class);
            startActivity(intent);
            finish();
        }
        else if(id== R.id.main_link)
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    protected String[] createSpinnerList(HashMap<String, ClassicShortcut> shortCuts)
    {
        String[] out = new String[shortCuts.size()+1];
        out[0] = "";
        List<String> keys = new ArrayList<String>(shortCuts.keySet());
        for(int i = 1; i < out.length; i++)
            out[i] = keys.get(i-1);
        return out;
    }
}
