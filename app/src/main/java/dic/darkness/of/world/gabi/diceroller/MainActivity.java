package dic.darkness.of.world.gabi.diceroller;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



import dic.darkness.of.world.gabi.diceroller.chronicle.ChroniclesRollerActivity;
import dic.darkness.of.world.gabi.diceroller.classic.ClassicRollerActivity;

/**
 * @author Gab
 * The MainActivity. Acts a menu for navigating the app.
 *
 * Code for about dialog is based on https://android.okhelp.cz/create-about-dialog-android-example/
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Button classicButton = findViewById(R.id.classic_link);
        classicButton.setEnabled(true);
        classicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchActivity(ClassicRollerActivity.class);
            }
        });

        final Button chronicleButton = findViewById(R.id.chronicles_link);
        chronicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchActivity(ChroniclesRollerActivity.class);
            }
        });

        final Button fifthButton = findViewById(R.id.fifth_link);
        fifthButton.setEnabled(false);

        View messageView = getLayoutInflater().inflate(R.layout.about, null, false);

        // When linking text, force to always use default color. This works
        // around a pressed color state bug.
        TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
        int defaultColor = textView.getTextColors().getDefaultColor();
        textView.setTextColor(defaultColor);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setView(messageView);

        final FloatingActionButton aboutButton = findViewById(R.id.fab);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();



            }
        });
    }

    private void launchActivity(Class<? extends AppCompatActivity> activityClass)
    {
        Intent intent = new Intent(getApplicationContext(), activityClass);
        startActivity(intent);
        finish();
    }

}
