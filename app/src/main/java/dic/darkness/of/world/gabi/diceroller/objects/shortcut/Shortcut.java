package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

/**
 * A shortcut to be saved in a json file to load a set of values for
 * a dice roll.
 */
public interface Shortcut
{
    public int getDiceNum();
}
