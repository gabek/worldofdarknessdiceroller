package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * A FileHandler for writing and reading a Shortcuts from a Json file.
 */
public abstract class AbstractShortcutHandler
{
    private Context context;
    private ObjectMapper objectMapper;
    private String filename;

    public AbstractShortcutHandler()
    {

    }

    protected AbstractShortcutHandler(Context context, String filename, ObjectMapper objectMapper)
    {
        this.context = context;
        this.filename = filename;
        this.objectMapper = objectMapper;
    }

    /**
     * Writes a @link{HashMap} containg Strings as keys and Shortcuts as values. To a json file.
     *
     * @param map A @link{HashMap} that contains String keys, and Shortcut Values.
     * @return boolean True indicates file was successfully written. False indicates failure.
     */
    public boolean writeFile(HashMap<String, ? extends Shortcut> map) {
        try {
            String json = objectMapper.writeValueAsString(map);
            FileOutputStream outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(json.getBytes());
            outputStream.close();
            return true;
        }
        catch (IOException e)
        {
          return false;
        }
    }

    protected Context getContext()
    {
        return context;
    }

    protected ObjectMapper getObjectMapper()
    {
        return objectMapper;
    }

    protected String getFilename()
    {
        return filename;
    }
}
