package dic.darkness.of.world.gabi.diceroller.objects.action;

/**
 * The Results of a roll as an Enumerator. System Agnostic.
 */
public enum Result
{
    Success("Success"),
    Critical("Critical Success"),
    Failure("Failure"),
    Botch("Botch"),
    Exceptional("Exceptional Success"),
    Dramatic("Dramatic Failure"),
    TotalFailure("Total Failure"),
    MessyCritical("Messy Critical"),
    BestialFailure("Bestial Failure");

    private final String value;

    private Result(String value)
    {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
