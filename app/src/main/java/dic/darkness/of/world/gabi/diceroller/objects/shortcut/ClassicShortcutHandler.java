package dic.darkness.of.world.gabi.diceroller.objects.shortcut;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * A File handler for ClassicShortcuts.
 * Extends AbstractShortcutHandler.
 */
public class ClassicShortcutHandler extends AbstractShortcutHandler
{
    private MapType mapType;

    public ClassicShortcutHandler()
    {
        super();
    }

    public ClassicShortcutHandler(Context context, String filename, ObjectMapper objectMapper)
    {
        super(context,filename, objectMapper);
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        mapType = typeFactory.constructMapType(HashMap.class, String.class, ClassicShortcut.class);
    }

    /**
     * Reads a json file and returns a HashMap of ClassicShortcuts.
     * If the file doesn't exist or fails will instead return an empty map.
     *
     * @return @link{HashMap} A map representing values stored in a JSON value.
     */
    public HashMap<String, ClassicShortcut> readFile()
    {
        try {
            FileInputStream input = super.getContext().openFileInput(super.getFilename());
            HashMap<String, ClassicShortcut> map = super.getObjectMapper().readValue(input, mapType);
            return map;
        }
        catch (IOException e)
        {
            return  new HashMap<String, ClassicShortcut>();
        }
    }

    /**
     * Writes a map to the specified file. Will remove any non ClassicShortcut
     * entries.
     *
     * @param map A @link{HashMap} containing Strings and Shortcuts.
     * @return boolean Represents if the file was successfully writen or not.
     */
    @Override
    public boolean writeFile(HashMap<String, ? extends Shortcut> map)
    {
        Set<String> keys = new HashSet<>(map.keySet());
        for (String key : keys)
        {
            if(!(map.get(key) instanceof  ClassicShortcut))
                map.remove(key);
        }
        return super.writeFile(map);
    }
}
