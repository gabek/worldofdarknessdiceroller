package dic.darkness.of.world.gabi.diceroller.objects.action;

import java.util.Random;

/**
 * A Generic Dice roller for rolling dice.
 */

public class DiceRoller
{
    private int diceSize;

    public DiceRoller()
    {
        diceSize = 10;
    }

    public DiceRoller(int diceSize)
    {
        this.diceSize = diceSize;
    }

    /**
     * Rolls a singular dice.
     *
     * @return int The result of the roll.
     */
    public int roll()
    {
        Random rand = new Random();
        return rand.nextInt(diceSize)+1;
    }

    /**
     * Rolls a set number of dice and returns their result.
     *
     * @param numberOfDice The size of the dice pool.
     * @return int[] The result of roll.
     */
    public int[] roll(int numberOfDice)
    {
        if(numberOfDice<0) return null;
        int[] out = new int[numberOfDice];
        for(int i=0; i<numberOfDice; i++)
            out[i] = roll();
        return out;
    }

    public int getDiceSize() {
        return diceSize;
    }

    public void setDiceSize(int diceSize) {
        this.diceSize = diceSize;
    }
}
