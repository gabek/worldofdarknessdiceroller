package dic.darkness.of.world.gabi.diceroller.chronicle;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Spinner;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ChronicleShortcut;

import static org.junit.Assert.assertEquals;

/**
 * Tests the ChronicleDeleteActivity setup.
 */
@RunWith(AndroidJUnit4.class)
public class ChronicleDeleteActivityTest
{
    @Rule
    public ActivityTestRule<ChronicleDeleteActivity> testRule = new ActivityTestRule<>(ChronicleDeleteActivity.class);

    private ChronicleDeleteActivity activity;

    @Before
    public void setUp()
    {
        activity = testRule.getActivity();
    }

    /**
     * Tests the Dropdown Element.
     */
    @Test
    public void testDropdown()
    {
        final Spinner dropdown = activity.findViewById(R.id.spinner_shortcut);

        String result = (String) dropdown.getSelectedItem();

        assertEquals("", result);
    }

    /**
     * Tests the creation of a spinner list.
     */
    @Test
    public void testCreateSpinnerList()
    {
        HashMap<String, ChronicleShortcut> map = new HashMap<>();
        map.put("Alpha", new ChronicleShortcut(8, 10));
        map.put("Beta", new ChronicleShortcut(3, 9));

        String[] result = activity.createSpinnerList(map);

        String[] expected = {"", "Alpha", "Beta"};

        assertEquals(expected.length, result.length);
        for(int i = 0; i < expected.length; i++)
            assertEquals(expected[i], result[i]);
    }


    @After
    public void tearDown()
    {
        activity = testRule.getActivity();
    }
}
