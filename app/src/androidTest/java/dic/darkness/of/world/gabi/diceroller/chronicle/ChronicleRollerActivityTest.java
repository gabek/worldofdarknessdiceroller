package dic.darkness.of.world.gabi.diceroller.chronicle;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ChronicleShortcut;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ClassicShortcut;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the ChrconicleRollerActivity.
 */
@RunWith(AndroidJUnit4.class)
public class ChronicleRollerActivityTest
{
    @Rule
    public ActivityTestRule<ChroniclesRollerActivity> testRule = new ActivityTestRule<>(ChroniclesRollerActivity.class);
    private ChroniclesRollerActivity activity;
    private HashMap<String, ClassicShortcut> map;


    @Before
    public void setUp()
    {
        activity = testRule.getActivity();
    }

    /**
     * Tests if clicking the button will change the TextView.
     */
    @Test
    public void testRollerButton()
    {
        final TextView textView = activity.findViewById(R.id.instructions);
        final Button rollerButton = activity.findViewById(R.id.roll_button);

        final String expectedBefore = "Roll the Dice";

        assertEquals(expectedBefore, textView.getText().toString());

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                rollerButton.performClick();
                assertNotEquals(expectedBefore, textView.getText().toString());
            }
        });


    }

    /**
     * Tests the number pickers default values.
     */
    @Test
    public void testNumberPickers()
    {
        final NumberPicker diceNum = activity.findViewById(R.id.dice_num);
        final NumberPicker rerollNum = activity.findViewById(R.id.reroll_num);
        final TextView diceText = activity.findViewById(R.id.dice_title);
        final TextView rerollText = activity.findViewById(R.id.reroll_title);

        int expectedDiceNum = 6;
        int expectedDiceMin = 1;
        int expectedDiceMax = 50;

        int expectedRerollNum = 10;
        int expectedRerollMin = 8;
        String expectedDice = "Number of Dice";
        String expectedReroll = "Re-roll On";

        assertEquals(expectedDiceNum, diceNum.getValue());
        assertEquals(expectedDiceMin, diceNum.getMinValue());
        assertEquals(expectedDiceMax, diceNum.getMaxValue());
        assertEquals(expectedRerollNum, rerollNum.getValue());
        assertEquals(expectedRerollMin, rerollNum.getMinValue());
        assertEquals(expectedRerollNum, rerollNum.getMaxValue());
        assertEquals(expectedDice, diceText.getText().toString());
        assertEquals(expectedReroll, rerollText.getText().toString());
    }

    /**
     * Tests the Spinner dropdown.
     */
    @Test
    public void testDropdown()
    {
        final Spinner dropdown = activity.findViewById(R.id.spinner_shortcut);

        final NumberPicker diceNum = activity.findViewById(R.id.dice_num);
        final NumberPicker rerollNum = activity.findViewById(R.id.reroll_num);

        final String result = (String) dropdown.getSelectedItem();

        assertEquals("", result);
        assertTrue(diceNum.isEnabled());
        assertTrue(rerollNum.isEnabled());

    }

    /**
     * Tests creating the spinner list.
     */
    @Test
    public void testCreateSpinnerList()
    {
        HashMap<String, ChronicleShortcut> map = new HashMap<>();
        map.put("Alpha", new ChronicleShortcut(8, 10));
        map.put("Beta", new ChronicleShortcut(3, 8));

        String[] result = activity.createSpinnerList(map);

        String[] expected = {"", "Chance Die", "Alpha", "Beta"};

        assertEquals(expected.length, result.length);
        for(int i = 0; i < expected.length; i++)
            assertEquals(expected[i], result[i]);
    }


    @After
    public void cleanUp()
    {
        activity.finish();
    }
}
