package dic.darkness.of.world.gabi.diceroller.classic;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import dic.darkness.of.world.gabi.diceroller.R;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ClassicSaveActivityTest
{
    @Rule
    public ActivityTestRule<ClassicSaveActivity> testRule = new ActivityTestRule<>(ClassicSaveActivity.class);
    private ClassicSaveActivity activity;

    @Before
    public void setUp()
    {
        activity = testRule.getActivity();
    }

    /**
     * Tests the number pickers.
     */
    @Test
    public void testNumberPickers()
    {
        final NumberPicker diceNum = activity.findViewById(R.id.dice_num);
        final NumberPicker diffNum = activity.findViewById(R.id.diff_num);
        final TextView diceText = activity.findViewById(R.id.dice_text);
        final TextView diffText = activity.findViewById(R.id.diff_text);

        int expectedNum = 6;
        int expectedMin = 1;
        int expectedDiceMax = 50;
        int expectedDiffMax = 10;
        String expectedDice = "Number of Dice";
        String expectedDiff = "Challenge Rating";

        assertEquals(expectedNum, diceNum.getValue());
        assertEquals(expectedMin, diceNum.getMinValue());
        assertEquals(expectedDiceMax, diceNum.getMaxValue());
        assertEquals(expectedNum, diffNum.getValue());
        assertEquals(expectedMin, diffNum.getMinValue());
        assertEquals(expectedDiffMax, diffNum.getMaxValue());
        assertEquals(expectedDice, diceText.getText().toString());
        assertEquals(expectedDiff, diffText.getText().toString());
    }

    /**
     * Tests the shortcut name.
     */
    @Test
    public void testShortcutName()
    {
        final EditText text = activity.findViewById(R.id.shortcut_name);

        String expectedText = "";
        String expectedHint = "Insert Name";
        int expectedLines = 1;

        assertEquals(expectedText, text.getText().toString());
        assertEquals(expectedHint, text.getHint().toString());
        assertEquals(expectedLines, text.getMaxLines());
    }

    @After
    public void tearDown()
    {
        activity.finish();
    }
}
