package dic.darkness.of.world.gabi.diceroller.chronicle;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import dic.darkness.of.world.gabi.diceroller.R;

import static org.junit.Assert.assertEquals;

/**
 * Tests the ChronicleSaveActivity.
 */
@RunWith(AndroidJUnit4.class)
public class ChronicleSaveActivityTest
{
    @Rule
    public ActivityTestRule<ChronicleSaveActivity> testRule = new ActivityTestRule<>(ChronicleSaveActivity.class);
    private ChronicleSaveActivity activity;

    @Before
    public void setUp()
    {
        activity = testRule.getActivity();
    }

    /**
     * Tests the NumberPickers.
     */
    @Test
    public void testNumberPickers()
    {
        final NumberPicker diceNum = activity.findViewById(R.id.dice_num);
        final NumberPicker rerollNum = activity.findViewById(R.id.reroll_num);
        final TextView diceText = activity.findViewById(R.id.dice_text);
        final TextView rerollText = activity.findViewById(R.id.reroll_text);

        int expectedNum = 6;
        int expectedDiceMin = 1;
        int expectedDiceMax = 50;

        int expectedRerollNum = 10;
        int expectedRerollMin = 8;
        String expectedDice = "Number of Dice";
        String expectedReroll = "Re-roll On";

        assertEquals(expectedNum, diceNum.getValue());
        assertEquals(expectedDiceMin, diceNum.getMinValue());
        assertEquals(expectedDiceMax, diceNum.getMaxValue());
        assertEquals(expectedRerollNum, rerollNum.getValue());
        assertEquals(expectedRerollMin, rerollNum.getMinValue());
        assertEquals(expectedRerollNum, rerollNum.getMaxValue());
        assertEquals(expectedDice, diceText.getText().toString());
        assertEquals(expectedReroll, rerollText.getText().toString());
    }

    /**
     * Tests the EditText of the Shortcut name.
     */
    @Test
    public void testShortcutName()
    {
        final EditText text = activity.findViewById(R.id.shortcut_name);

        String expectedText = "";
        String expectedHint = "Insert Name";
        int expectedLines = 1;

        assertEquals(expectedText, text.getText().toString());
        assertEquals(expectedHint, text.getHint().toString());
        assertEquals(expectedLines, text.getMaxLines());
    }


    @After
    public void tearDown()
    {
        activity.finish();
    }
}
