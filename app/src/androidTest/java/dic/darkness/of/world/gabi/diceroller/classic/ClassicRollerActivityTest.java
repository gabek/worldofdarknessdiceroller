package dic.darkness.of.world.gabi.diceroller.classic;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Button;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

import dic.darkness.of.world.gabi.diceroller.R;
import dic.darkness.of.world.gabi.diceroller.objects.shortcut.ClassicShortcut;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotEquals;

/**
 *
 */
@RunWith(AndroidJUnit4.class)
public class ClassicRollerActivityTest
{
    @Rule
    public ActivityTestRule<ClassicRollerActivity> testRule = new ActivityTestRule<>(ClassicRollerActivity.class);
    private ClassicRollerActivity activity;

    @Before
    public void setUp()
    {
        activity = testRule.getActivity();
    }

    /**
     * Tests if the roller button will change the Edit text when clicked.
     */
    @Test
    public void testRollerButton()
    {
        final TextView textView = activity.findViewById(R.id.instructions);
        final Button rollerButton = activity.findViewById(R.id.roll_button);

        final String expectedBefore = "Roll the Dice";

        assertEquals(expectedBefore, textView.getText().toString());

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                rollerButton.performClick();
                assertNotEquals(expectedBefore, textView.getText().toString());
            }
        });
    }

    /**
     * Tests the number pickers.
     */
    @Test
    public void testNumberPickers()
    {
        final NumberPicker diceNum = activity.findViewById(R.id.dice_num);
        final NumberPicker diffNum = activity.findViewById(R.id.diff_num);
        final TextView diceText = activity.findViewById(R.id.dice_title);
        final TextView diffText = activity.findViewById(R.id.diff_title);

        int expectedNum = 6;
        int expectedMin = 1;
        int expectedDiceMax = 50;
        int expectedDiffMax = 10;
        String expectedDice = "Number of Dice";
        String expectedDiff = "Challenge Rating";

        assertEquals(expectedNum, diceNum.getValue());
        assertEquals(expectedMin, diceNum.getMinValue());
        assertEquals(expectedDiceMax, diceNum.getMaxValue());
        assertEquals(expectedNum, diffNum.getValue());
        assertEquals(expectedMin, diffNum.getMinValue());
        assertEquals(expectedDiffMax, diffNum.getMaxValue());
        assertEquals(expectedDice, diceText.getText().toString());
        assertEquals(expectedDiff, diffText.getText().toString());
    }


    /**
     * Tests the drop down.
     */
    @Test
    public void testDropdown()
    {
        final Spinner dropdown = activity.findViewById(R.id.spinner_shortcut);

        String result = (String) dropdown.getSelectedItem();

        assertEquals("", result);
    }

    /**
     * Tests the creation of a spinner list.
     */
    @Test
    public void testCreateSpinnerList()
    {
        HashMap<String, ClassicShortcut> map = new HashMap<>();
        map.put("Alpha", new ClassicShortcut(8, 4));
        map.put("Beta", new ClassicShortcut(3, 7));

        String[] result = activity.createSpinnerList(map);

        String[] expected = {"", "Alpha", "Beta"};

        assertEquals(expected.length, result.length);
        for(int i = 0; i < expected.length; i++)
            assertEquals(expected[i], result[i]);


    }

    @After
    public void cleanUp()
    {
        activity.finish();
    }
}
